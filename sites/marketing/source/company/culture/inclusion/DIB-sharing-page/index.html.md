---
layout: markdown_page
title: "Diversity, Inclusion & Belonging Sharing Page"
description: "This page provides a place for team members to share any learnings for Ally Learning Groups, DIB Rountables and other DIB initiatives."
canonical_path: "/company/culture/inclusion/DIB-sharing-page/"
---

## Diversity, Inclusion & Belonging Sharing Page 

### Privilege For Sale Sharing 

**Example:**
- Group Members: 
- Privileges Chosen: 
- Reason for Privileges Chosen:
- Learnings from the experience: 

### DIB Roundtable Sharing

**Example:**
- Group Members
- What is one thing as a group can we do to make a positive impact on Diversity, Inclusion and Belonging?:
- What did the group learn from the experience?: 

### Ally Lab Learning Groups Activity Sharing 

**Example:**
- Group Members:
- Activity:
- Learnings, Actions, or Answers etc:  

### Ally Lab Learning Groups Committment and Values pledge Sharing

**Example:**
- Group Members
- Committment: As a group we commit to 
- Values: The values we have chosen to ensure we align to out committment are as follows: Example: Action: We will say something when we see a DIB issue
