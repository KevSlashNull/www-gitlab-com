---
layout: handbook-page-toc
title: "Hacker News"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Hacker News is an important social channel. Threads that mention GitLab's structure, values, product vision, or other sensitive blog posts, articles, etc. should be treated as important, while posts about GitLab that land on the front page of Hacker News should be treated as both important and urgent.

GitLab mentions on Hacker News are tracked on the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel. Posts about GitLab that land on the front page of Hacker News generate a notification that is shared in [#community-relations](https://gitlab.slack.com/messages/community-relations).

## What performs well 

In 2020, we ran multiple experiments to try to drive interest in new posts about GitLab. We learned that the key to a post reaching the front page was the content itself. Creating a coordinated response was not effective in driving interest in the posts without organic interest from the Hacker News community. The content is the key to success. 

We conducted an audit to see what types of posts about GitLab landed on the front page of Hacker News. We learned that the following types of GitLab-related content generated the most interest on Hacker News: 
- Major company news such as announcements about funding, acquisitions, or moving features to core 
- Release posts 
- Announcements from our open source program members including posts from Gnome, KDE, and WikiMedia and other open-source focused content 
- Technical blog posts
- Remote-focused content including handbook pages and the Remote Work report 

When considering what types of content to publish on social media, these are all good types of posts and pages to share on Twitter, LinkedIn, relevant Slack communities, and other social channels. 

## Release days

GitLab Release Posts frequently perform well on Hacker News. Given that we know the post will be released each month on the 22nd, we should always plans for these posts to warrant a [community response](/handbook/marketing/community-relations/developer-evangelism/community-response/). For release days, we follow these steps to organize our response: 

1. Manager, Developer Evangelism will send out a recurring calendar invite for release days with a link to this section of the handbook in the body of the invite as a reminder to Developer Evangelism team members.
1. When release day (the 22nd day of the month) falls on the weekend, the Developer Evangelism team will assign a Developer Evangelist to be on-call to address community concerns. That person should take off the following Monday or another day of their choosing to offset the time spent working over the weekend.  
  - In 2021, release day falls on the weekend on Saturday, May 22 and Sunday, August 22.
  - The Developer Evangelist team should coordinate with the [Release Post Managers schedule](/handbook/marketing/blog/release-posts/managers/) to determine when the release post will be published. This is important as the Developer Evangelist who is on-call should be able to respond to comments within 30 minutes for 12 hours following the publishing of the post.
  - The Developer Evangelist on-call should have notifications enabled so that they receive an alert from Slack if the "Hacker News Front Page" bot posts a message in the #developer-evangelism Slack channel. 
1. If a notification that a release post has reached the Hacker News Front Page is posted to the #developer-evangelism Slack channel by the "Hacker News Front Page" bot, a Developer Evangelism team member should quickly repost the message to the #product and #release-post channels to alert our Product Managers. 
1. As questions / comments are added to the Hacker News post, Developer Evangelists should quickly respond to address community feedback.
1. For questions / comments that the Developer Evangelists are unable to address on their own, they should quickly activate to engage experts. The most efficient way to engage experts are to: notify product managers via `@` mentions in the thread created in the #product channel with a link to the comment which they should address. Note: Don't be shy about engaging experts as the community feedback is valuable to them as they work to improve GitLab. 

More information on [Release Posts](/handbook/marketing/blog/release-posts/), including the [Release Post Managers schedule](/handbook/marketing/blog/release-posts/managers/), is available in the handbook. 

## Responding on Hacker News 

When new posts or comments about GitLab are added to Hacker News, team members can find alerts about these posts in the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel. You are welcome to share these with other team members and members of the wider GitLab community who may want to engage in the discussion. Developer Evangelism team members are encouraged to address comments and questions on new posts when appropriate. 

### Response workflow

1. When alerted by the "Hacker News Front Page Bot" that an article referencing GitLab is on the Front Page of Hacker News, a Developer Evangelists should coordinate to ensure a DRI is assigned to the post to review, monitor the comments, and respond accordingly. 
1. Developer Evangelists should also review the #hn-mentions Slack channel 1-2 times per day for mentions that require a response. If you are able to provide a quick response/resource, reply on [news.ycombinator.com](https://news.ycombinator.com) using your personal Hacker News account and indicate that you replied by leaving a :white_check_mark: on the Slack comment. _Note that [the window to edit a comment is 2 hours](https://github.com/minimaxir/hacker-news-undocumented#editdelete-time-limits), afterwards you cannot edit or delete a Hacker News comment._
1. If necessary, you may also wish to share comments with relevant experts who may be able to provide more detailed or insightful comments. This can be done by sharing relevant posts or comments in an appropriate Slack channel if you judge additional input is required.

### Best practices when responding on Hacker News

When responding to a post about GitLab on Hacker News:

- Don't post answers that are almost the same, link to the original one instead.
- Address multi-faceted comments by breaking them down and using points, numbering and quoting.
- When someone posts a Hacker News thread link, monitor that thread manually. Don't wait for the notifications in the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) channel, because sometimes they're delayed by a few hours.

## Social media guidelines

- Never submit GitLab content to Hacker News. Submission gets more credibility if a non-GitLab Hacker News community member posts it, we should focus on making our posts interesting instead of on submitting it.
- Don't share links to Hacker News stories/comments on Slack or Twitter and ask others to upvote because it might set off the voting detector.
- Don't make the first comment on a Hacker post, allow people to leave comments and ask questions.
- Avoid using corporate jargon like 'PeopleOps'.
- Always address the Hacker News community as peers. Be sure to always be modest and grateful in responses.
- If you comment yourself make sure it is interesting and relevant.
- Make yourself familiar with [A List of Hacker News's Undocumented Features and Behaviors](https://github.com/minimaxir/hacker-news-undocumented) to understand Hacker News behaviour and moderation rules.
- Check the tone of your response: Don’t be defensive, but instead share your point of view. 
- Try to teach people something interesting they didn’t know already.
- Add value to your post with data points or direct links.

Note: You can find the full list of social media guidelines [here](/handbook/marketing/social-media-guidelines/)

## Automation

Hacker News mentions are piped to Community Zendesk and the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel by [Zapier](https://zapier.com).
