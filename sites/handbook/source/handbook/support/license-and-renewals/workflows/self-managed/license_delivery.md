---
layout: markdown_page
title: Delivering License Files
description: "How to deliver generated license files"
category: GitLab Self-Managed licenses
---

{:.no_toc}

----

# Overview

Frequently we need to generate license files, the request for those can be originated in:

1. By a customer direct request in Zendesk
1. As part of an internal request

In both cases the license can be attached to the issue or ticket where the request was made.

However, once read-only access is available for LicenseDot (see https://gitlab.com/gitlab-org/license-gitlab-com/-/issues/204), this workflow should be updated (see https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3251 for a discussion on this topic).
