---
layout: handbook-page-toc
title: "EKS Container Sandbox"
description: "The GitLab Demo Systems EKS container environment handbook pages provides an overview of how our AWS cluster infrastructure is configured and has answers to frequently asked questions."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview 

We have a AWS project (`gitlab-np`) for all Customer Success team members that is part of the GitLab consolidated billing organization.

In the near future, the Demo Systems team will offer automated provisioning of EKS clusters. In the meantime, please use the AWS Compute Sandbox to provision your own EKS cluster.

In general, EKS clusters should be destroyed after you are finished with your testing.

