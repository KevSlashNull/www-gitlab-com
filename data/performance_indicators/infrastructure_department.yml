- name: Infrastructure Hiring Actual vs Plan
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan"
  definition: Employees are in the division "Engineering" and department is "infrastructure".
  target: greater than 0.9
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - Recruiting pipeline and hiring velocity has drastically improved. 3 new SREs have started in the last month and 1 offer is currently pending.
    - Putting 1 DBRE position on hold to fund 1 Trust & Safety team position.
    - Currently hiring for 7 positions (2x DBRE, 2x SRE, 3x SRE Manager).
  sisense_data:
    chart: 11488988
    dashboard: 851270
    embed: v2
  urls:
  - "/handbook/hiring/charts/infrastructure-department/"
- name: Infrastructure Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a responsible business to be successful, and to one day go on the public market.
  target: Identified in Sisense Chart
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - Currently running approximately 19% of plan YTD as of end Feb
- name: GitLab.com Availability
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Percentage of time during which GitLab.com is fully operational and providing service to users within SLO parameters. Definition is available on the <a href="https://gitlab.com/gitlab-com/dashboards-gitlab-com/-/metrics/sla-dashboard.yml?environment=1790496&duration_seconds=2592000">GitLab.com Service Level Availability page</a>.
  target: equal or greater than 99.95%
  org: Infrastructure Department
  is_key: true
  urls:
  - https://gitlab.com/gitlab-com/dashboards-gitlab-com/-/metrics/sla-dashboard.yml?environment=1790496&duration_seconds=2592000
  health:
    level: 2
    reasons:
    - Feb 2021 Availability 99.87%
    - Jan 2021 Availability 99.88%
    - Dec 2020 Availability 99.96%
    - Nov 2020 Availability 99.90%
    - Oct 2020 Availability 99.74%
    - Sept 2020 Availabilty 99.95%
    - Aug 2020 Availability 99.87%
    - July 2020 Availability 99.81%
    - June 2020 Availability 99.56%
    - May 2020 Availability 99.58%
- name: Mean Time To Production (MTTP)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the elapsed time (in hours) from merging a change in <a href="https://gitlab.com/gitlab-org/gitlab">gitlab-org/gitlab projects</a> master branch, to deploying that change to gitlab.com. It serves as an indicator of our speed capabilities to deploy application changes into production.
  target: less than 24 hours
  org: Infrastructure Department
  is_key: true
  urls:
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/170
  health:
    level: 3
    reasons:
    - Due to continued increased deployment delays continuing from previous months, in March 2021 the target has been increased to 24 hours. We will <a href="https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/65">consider lowering the target</a> once we are able to deploy without any interruptions for two consecutive weeks.
    - MTTP increases in Jan. and Feb. 2021 are a result of the <a href="https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl">PCL</a> and <a href="https://gitlab.com/gitlab-com/gl-infra/production/-/issues/3487"> deployment-impacting migration timeouts</a>.
    - In September 2020, the target is lowered to 12 hours (previously 24).
    - The next step for this metric is to move from Continuous Delivery to Continuous Deployment for GitLab.com. Work is tracked in <a href="https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/280"> epic 280 </a>.
    - We will consider lowering the target once we achieve three consecutive months below the target AND when we introduce (and successfully use) automated rollbacks for three consecutive months.
  sisense_data:
    chart: 10055732
    dashboard: 764878
    embed: v2
- name: Mean Time Between Failure (MTBF)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the average time (in hours) between failures for any service. Higher values are better. Failure is a breach of the MTBF threshold which is higher than our Production monitoring threshold, and in turn, higher than our contractual SLO thresholds. MTBF is an internal system measurement used as an indicator on whether focus is necessary for individual services, used in combination with <a href="/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-individual-service-maturity"> GitLab.com service maturity </a>. MTBF is not a measure of the customer perceived impact, for that see <a href="/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability">GitLab.com Availability</a> and <a href="/handbook/engineering/infrastructure/performance-indicators/#mean-time-between-incidents-mtbi"> MTBI</a>.
  target: 6 hours.
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/941
  - https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/876
  - https://gitlab.com/gitlab-data/analytics/-/issues/7713
  - https://dashboards.gitlab.com/d/general-mtbf/general-mean-time-between-failure?orgId=1
  chart_image: https://gitlab.com/api/v4/projects/14231319/jobs/artifacts/master/raw/global-gitlab-com-mtbf.png?job=refresh-mtbf-graph
  health:
    level: 2
    reasons:
    - The initial target is set in March 2021.
    - Chart image from Grafana is used because <a href="https://gitlab.com/gitlab-data/analytics/-/issues/7713#note_524683371">we are not able to add Thanos as a datasource for Sisense</a>.
    - Metric is update to reflect new method of using multi-window-multi-burn rates.
    - Discussion continues in <a href="https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/941">the issue.</a>
    - This metric was re-introduced in October 2020.

- name: Mean Time Between Incidents (MTBI)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the mean elapsed time (in hours) from the start of one production incident, to the start of the next production incident. It serves primarily as an indicator of the amount of disruption being experienced by users and by on-call engineers. This metric includes only <a href="https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability">Severity 1 & 2 incidents</a> as these are most directly impactful to customers. This metric can be considered "MTBF of Incidents".
  target: more than 120 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://app.periscopedata.com/app/gitlab/764913/MTBF-KPI-Dashboard?widget=10056392&udv=0
  health:
    level: 1
    reasons:
    - This metric updated in December 2020 to scope to only Severity 1 & 2 incidents as these are most impactful to customers.
    - Metric update also changed the target from 24 hours to 120 hour with the intent that we should not have such incidents more than approximately weekly (hopefully less).  Furter iterations will increase this target when we incorporate environment (production only).
    - Deployment failures (and the mean time between them) will be extracted into a separate metric to serve as a quality countermeasure for MTTP, unrelated to this metric which focuses on declared service incidents.
  sisense_data:
    chart: 10056392
    dashboard: 764913
    embed: v2
- name: GitLab.com Hosting Cost per GitLab.com Unique Monthly Active Users
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric reflects an estimate of the dollar cost necessary to support one user in GitLab.com. It is an important metric because it allows us to estimate infrastructure costs as our user base grows. GitLab.com   Hosting Cost comes from Netsuite; it is a sum of actual amounts within GitLab.com:Marketing and GitLab.com departments, with account numbers of 5026 or 6026. This cost is divided by <a href="/handbook/product/performance-indicators/#unique-monthly-active-users-umau">UMAU</a>
  target: less than .80
  public: false
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/764957/Infrastructure-Hosting-Cost-UMAU-KPI
  health:
    level: 2
    reasons:
    - See notes in Key Agenda Doc.
- name: GitLab.com Hosting Cost per Free GitLab.com Unique Monthly Active Users
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric reflects an estimate of the dollar cost necessary to support one user of GitLab.com at the Free tier. GitLab.com  Hosting Cost comes from Netsuite; it is a sum of actual amounts within GitLab.com:Marketing department, with account numbers of 5026 or 6026. This cost is divided by GitLab.com Free UMAU (Total UMAU minus total paid licensed users).
  target: less than .50
  public: false
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/764957/GitLab.com-Hosting-Cost-UMAU-KPI
  health:
    level: 2
    reasons:
    - See notes in Key Agenda Doc.
- name: GitLab.com Hosting Cost per Paid GitLab.com Licensed User
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric reflects an estimate of the dollar cost necessary to support one user of GitLab.com at the paid tiers. GitLab.com Hosting Cost comes from Netsuite; it is a sum of actual amounts within GitLab.com department, with account numbers of 5026 or 6026. This cost is divided by total paid licensed users on GitLab.com.
  target: less than 3
  public: false
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/764957/GitLab.com-Hosting-Cost-UMAU-KPI
  health:
    level: 2
    reasons:
    - See notes in Key Agenda Doc.
- name: Infrastructure Average Location Factor
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously, and hiring great people in low-cost regions where we pay market rates. We track an average location factor by function and department so managers can make tradeoffs and hire in an expensive region when they really need specific talent unavailable elsewhere, and offset it with great people who happen to be in low cost areas.
  target: equal or less than 0.58
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are currently above our target after the shift of location factor to geo regions. We are also doing significant hiring and have focused part of our hires in APAC, allowing opportunities for some more efficient geo region hires.
  sisense_data:
    chart: 11490180
    dashboard: 851333
    embed: v2
- name: Infrastructure Department New Hire Average Location Factor
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously, and hiring great people in low-cost regions where we pay market rates. We track an average location factor for team members hired within the past 3 months so hiring managers can make tradeoffs and hire in an expensive region when they really need specific talent unavailable elsewhere, and offset it with great people who happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: 0.58
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - Recent new hires have had slightly upward pressure on the metric. A couple of our pending roles for hire will provide opportunity for more efficient geo zone hires in APAC.
  sisense_data:
    chart: 9389296
    dashboard: 719549
    embed: v2
- name: GitLab.com individual service maturity
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric aims to show the level of confidence we have of our insight into the significant GitLab services on GitLab.com. We are looking to capture the maturity on the scale of "manual check is necessary" to "the system informs us of an upcoming scaling need". This PI is paired with MTBF. For a service of highest maturity, decreased MTBF will show a need for additional scaling work on the service. For a service of lowest maturity, increased MTBF will require work on raising the maturity of the service.
  target: TBD
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/296
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/382
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/398
  health:
    level: 1
    reasons:
    - We are automating the data collection to simplify the process. Details are in <a href="https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/398">this epic</a>.
    - In January 2021, we began to implement a Service Maturity Model. The effort is manual and is being <a href="/handbook/engineering/infrastructure/team/scalability/#maturity-model">tracked in the handbook</a>, before introducing a target and associated metric.
    - In October, November and December 2020, we found that the missing part of tracking the scaling bottlenecks is the understanding of the service maturity and how the service contributes to system failures. We introduced MTBF in October as a counterpart to service maturity.
- name: Infrastructure Overall Handbook Update Frequency Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/index.html#infrastructure-handbook-update-frequency"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping up our transparency, and to recruiting successfully. Our processes are constantly evolving and we need a way to make sure the handbook is being updated at a regular cadence. This data is retrieved by querying the API with a python script for merge requests that have files matching `/source/handbook/engineering/**` or `/source/handbook/support/**` over time.
  target: 1.2
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - Continuing to watch this metric as well as work to implement the data changes which will incorporate more of our [activity from ops.gitlab.net](https://gitlab.com/gitlab-data/analytics/-/issues/2654).
  sisense_data:
    chart: 10586746
    dashboard: 621063
    shared_dashboard: b69578ca-d4a6-4a99-b06f-423a3683446c
    embed: v2
- name: Infrastructure Discretionary Bonus Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/index.html#infrastructure-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual GitLab team members who really shine as they live our values. Our goal is to award discretionary bonuses to 10% of GitLab team members in the Infrastructure department every month.
  target: near 10%
  org: Infrastructure Department
  is_key: false
  urls:
  - https://app.periscopedata.com/app/gitlab/850662/Infrastructure-Discretionary-Bonus-Rate-KPI-Dashboard
  health:
    level: 3
    reasons:
    - metric has been close to 10% for past 3 months (Jan 2021 - Mar 2021)
  sisense_data:
    chart: 11477307
    dashboard: 850662
    embed: v2
- name: Mean Time To Resolution (MTTR)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: For all <a href="/handbook/engineering/monitoring/#gitlabcom-service-level-availability"> customer-impacting services</a>, measures the elapsed time (in hours) it takes us to resolve when an incident occurs. This serves as an indicator of our ability to execute said recoveries. This includes Severity 1 & Severity 2 incidents from <a href="https://gitlab.com/gitlab-com/gl-infra/production">production project</a>
  target: less than 24 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/gl-infra/production/-/boards/1717012?&label_name[]=incident
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8296
  - https://app.periscopedata.com/app/gitlab/766694/MTTR-KPI
  health:
    level: 2
    reasons:
    - data depends on SREs adding incident::resolved label
  sisense_data:
    chart: 10083860
    dashboard: 766694
    embed: v2
- name: Mean Time To Mitigate (MTTM)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: For all <a href="/handbook/engineering/monitoring/#gitlabcom-service-level-availability"> customer-impacting services</a>, measures the elapsed time (in hours) it takes us to mitigate when an incident occurs. This serves as an indicator of our ability to mitigate production incidents. This includes Severity 1 & Severity 2 incidents from <a href="https://gitlab.com/gitlab-com/gl-infra/production">production project</a>
  target: less than 1 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://app.periscopedata.com/app/gitlab/766694/MTTR-KPI
  health:
    level: 2
    reasons:
    - data depends on SREs adding incident::mitigate label
  sisense_data:
    chart: 10083556
    dashboard: 766694
    embed: v2
- name: Infrastructure Department Narrow MR Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Infrastructure Department <a href="/handbook/engineering/#merge-request-rate">MR Rate</a> is a performance indicator showing how many changes the Infrastructure team implements in the GitLab product, as well as changes in support of the GitLab SaaS infrastructure.  We currently count all members of the Infrastructure Department (Directors, Managers, ICs) in the denominator, because this is a team effort. The <a href="/handbook/engineering/merge-request-rate/#projects-that-are-part-of-the-product">projects that are part of the product</a> contributes to the overall product development efforts.
  target: Greater than TBD MRs per month
  org: Infrastructure Department
  is_key: false
  health:
    level: 1
    reasons:
    - We have known projects which are not currently included in the measure. [Working with the Data team to resolve this](https://gitlab.com/gitlab-data/analytics/-/issues/2654).
    - We intend to observe this metric over time to understand the behavior of the differing work patterns and MR content (product code + infrastructure logic + configuration data).
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4445
  sisense_data:
    chart: 8934544
    dashboard: 686934
    shared_dashboard: 178a0fbc-42a9-4181-956d-a402151cf5d8
    embed: v2
- name: Corrective Action SLO
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: The Corrective Actions (CAs) SLO focuses on the number of open severity::1/severity::2 Corrective Action Issues past their due date. All Corrective Actions are in scope, regardless of department or project. CAs and their due dates are defined in our <a href="/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership">Incident Review process</a>.
  target: 0
  org: Infrastructure Department
  is_key: false
  health:
    level: 1
    reasons:
    - Due to the growing backlog of Corrective Actions, we are shifting towards more tech debt work in Q1, starting with OS and instance upgrades.
    - We started tracking this in July 2020. Many of our Corrective Actions do not have a proper Severity assigned yet (96 as of 2021/02/23)
  urls:
    - https://about.gitlab.com/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership
    - https://app.periscopedata.com/app/gitlab/787921/WIP:-Corrective-Actions
  sisense_data:
    chart: 10506729
    dashboard: 787921
    shared_dashboard: 178a0fbc-42a9-4181-956d-a402151cf5d8
    embed: v2
- name: Production Access Risk Index
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: The Production Access Risk Index provides a representative measure of the amount of access Team Members have to systems or sources of <a href="https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html#red">Red or Orange data</a> as well as underlying infrastructure control systems which could directly impact this data. Current Index includes employee access counts on read only database access weighted at 18, read/write rails console access as 10, and GCP production project access as 18.
  target: TBD
  org: Infrastructure Department
  is_key: false
  urls:
  - https://app.periscopedata.com/app/gitlab/792844/Infra-Prod-Console-Access
  - https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11179
  health:
    level: 2
    reasons:
    - Current index is NOT updated for Jan or Feb results - contention for the manual work required on this vs. other priorities. Three SRE Manager roles to fill will help improve work on this and other similar metrics.
    - Current index is an MVC started in Dec 2020.
    - Future iterations to include more systems and further refinement of weighting.
    - Started tracking some key access base metrics in Sept.
  sisense_data:
    chart: 10520523
    dashboard: 792844
    embed: v2
