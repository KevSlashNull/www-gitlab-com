## Basic setup

1. [ ] Change this issue title to include your name: `Trainee Quality Maintainer: [Your Name]`.
1. [ ] Choose which Quality project(s) you would like to become a maintainer:
   - [ ] [`GitLab (/qa)`](https://gitlab.com/gitlab-org/gitlab/-/tree/master/qa)
   - [ ] [`GitLab-QA`](https://gitlab.com/gitlab-org/gitlab-qa)
   - [ ] [`GitLab Triage`](https://gitlab.com/gitlab-org/gitlab-triage/)
   - [ ] [`Triage Ops`](https://gitlab.com/gitlab-org/quality/triage-ops/)
   - [ ] [`GitLab CustomersDot (/qa)`](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/staging/qa/)
1. [ ] The requirements for each project is different, please make sure to read the
[maintainer section in the Quality handbook](https://about.gitlab.com/handbook/engineering/quality/project-management/#reviewers-and-maintainers).
1. [ ] Add yourself as a [trainee maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer)
in the [team database](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md).
1. [ ] Mention your manager in this issue for awareness.

### Merge Request Authored

<!--
  Please list all MRs you have authored during or before your time as a Trainee Maintainer,
  for example:
  1. Framework Improvements: <link_to_mr>
  2. Quality e2e Tests: <link_to_mr>
-->

### Merge Request Reviewed

After each MR that you reviewed is merged or closed, add a discussion to this issue using this
template:

```markdown
### (Merge request title): (Merge request URL)

During review:

- (List anything of note, or a quick summary. "I suggested/identified/noted...")

Post-review:

- (List anything of note, or a quick summary. "I missed..." or "Merged as-is")

(Maintainer who reviewed this merge request) Please add feedback.
```

**Note:** Do not include reviews of security MRs because review feedback might
reveal security issue details.

**Tip:** There are [tools](https://about.gitlab.com/handbook/tools-and-tips/#trainee-maintainer-issue-upkeep) available to assist with this task.

## When you're ready to make it official

The trainee should also feel free to discuss their progress with their manager
or any maintainer at any time.

1. [ ] Create a merge request updating your [team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md)
proposing yourself as a quality maintainer for the chosen project.
1. [ ] Keep reviewing, start merging 😃

/label ~"trainee maintainer" ~"Quality Department"
