This checklist is based on the Inbound Marketing Team's documentation for [reviewing merge requests](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/merge-requests/).

- [ ] Contents of the MR align with our [GitLab company values](https://about.gitlab.com/handbook/values/).
- [ ] All items necessary to measure conversion impact have been accounted for.
- [ ] All interaction states have been tested such as active, focus, and hover.
- [ ] All images, PDFs, and other files have been optimized and follow our [image guidelines](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/image-guidelines/).
- [ ] All other [guidelines](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/merge-requests/#guidelines) have been taken under advisement.
- [ ] **You have searched for any pages using the updated files to ensure those pages won't be negatively impacted by these changes (ie regression tests).**
- [ ] This page behaves as expected in all supported browsers (Chrome [Blink], Safari [Webkit], and Firefox[Gecko]).
- [ ] This page behaves as expected on phone, tablet, laptop, and desktop screen sizes.
- [ ] This MR works on a touchscreen (important information isn't contained solely within hover events).
- [ ] This MR works with cookiebot.
- [ ] All console logs have been disabled before releasing to production and there are no javascript errors on the page.
- [ ] All buttons and links go to where they're supposed to go.
