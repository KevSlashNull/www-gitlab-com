---
layout: markdown_page
title: "Category Direction - Continuous Integration Scaling"
description: "Lean more about how we are scaling Continuous Integration on GitLab.com!"
canonical_path: "/direction/verify/continuous_integration_scaling/"
---

- TOC
{:toc}

## Continuous Integration Scaling 

In FY22, we have committed to [SaaS First](/direction/enablement/#saas-first). In the Verify Stage, this means prioritizing the scale of Continuous Integration and ensuring our users on GitLab.com are leveraging a reliable and available service. We are focused on a goal of 20M builds per day as it represents not only a target to drive our future architecture, but a volume that we expect to achieve within several quarters.

## Additional Resources

- [Maturity Plan](#maturity-plan)
- [Issue List]()
- [Overall Vision of the Verify stage](/direction/ops/#verify)

For specific information and features related to authoring/defining pipelines, check out [Pipeline Authoring](/direction/verify/pipeline_authoring). You may also be looking for one of the following related product direction pages: [GitLab Runner](/direction/verify/runner/).

## What's Next & Why

To begin working towards our goal, we will focus on building load testing environments in which we can perform measured tests around the initial primary architecture changes listed below:

1. [Improve GitLab CI/CD data model](https://gitlab.com/gitlab-org/architecture/tasks/-/issues/5)
1. [Improve Runner job queuing](https://gitlab.com/gitlab-org/gitlab/-/issues/322972)
1. [GitLab Runner Autoscaler architecture](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/57051)

## Architectural Overview 

## Challenges 






